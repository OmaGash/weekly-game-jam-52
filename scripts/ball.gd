extends RigidBody2D

var audio_coin = [
            preload("res://audio/coin1.ogg"),
			preload("res://audio/coin2.ogg"),
			preload("res://audio/coin3.ogg")
            ]
var audio_hit = [
					preload("res://audio/hit1.ogg"),
					preload("res://audio/hit2.ogg"),
					preload("res://audio/hit3.ogg")
					]

var audio_wall = [
					preload("res://audio/wall1.ogg"),
					preload("res://audio/wall2.ogg"),
					preload("res://audio/wall3.ogg")
]

var audio_fall = preload("res://audio/fall.ogg")

onready var sfx = get_node("AudioStreamPlayer")

const ACCELERATION = 400
const MAX_SPEED = 30000

func _ready():
	t.get_main_node().find_node("paddle").no_balls = false
	set_physics_process(true)
	randomize()

func _physics_process(delta):
	
	var bodies = get_colliding_bodies()
	
	
	
	#Loops through every body that contacts the ball
	for body in bodies:
		
		#If it belongs to a brick group(basically everything that is in the brick group is most likely a brick)
		if body.is_in_group("brick"):
			var timer = get_node("../ui/timer")
			
			#If the timer is still running, append more time.
			if !timer.is_stopped():
				var new = timer.time_left + 1#min(timer.time_left, timer.wait_time) + 1
				#timer.stop()
				timer.wait_time = new
				timer.start()
			t.get_main_node().find_node("label_score").display += 3
			
			sfx.stream = audio_hit[int(rand_range(0.1,len(audio_hit) - 0.1))]
			sfx.stream.loop = false
			sfx.play()
			
			body.queue_free()
		
		elif body.is_in_group("boss"):
			
			body.hp -= 1
		
		#Change the condition according to the paddle's node name
		elif body.get_name() == "paddle":
			var speed = linear_velocity.length()
			var dir = position - body.get_node("point").global_position
			var velocity = dir.normalized() * min(speed + ACCELERATION * delta, MAX_SPEED * delta)
			print(linear_velocity)
			linear_velocity = velocity
		
		#This will most likely be a wall
		else:
			sfx.stream = audio_wall[int(rand_range(0.1,len(audio_wall) - 0.1))]
			sfx.stream.loop = false
			sfx.play()
	
	#If the ball gets out of the viewport
	if self.position.y > get_viewport_rect().end.y:
		
		
		if t.get_main_node().find_node("label_life").hp <= 0:
			var timer = get_node("../ui/timer")
			timer.emit_signal("timeout")
			timer.stop()
		t.get_main_node().find_node("paddle").no_balls = true
		
		sfx.stream = audio_fall
		sfx.stream.loop = false
		sfx.play()
		
		
		self.queue_free()
