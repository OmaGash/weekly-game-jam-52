extends Timer

func _ready():
	t.get_main_node().get_node("paddle").connect("go", self, "go")
	set_process(true)
	get_node("time").hide()

var went = false

func _process(delta):
	$time.text = "Timer: "+str(int(time_left))
	
	if is_stopped() and t.get_main_node().find_node("paddle").no_balls and time_left <= 0:
		$time.text = "Time is up!"
	elif is_stopped() and t.global_find_node("label_life").hp <= 0 and t.global_find_node("paddle").no_balls:
		$time.text = "Game\nOver!"

#Start the timer once the go signal is emitted.
func go():
	#if is_stopped() and !went:
		#went = true
	if wait_time != 5:
		wait_time = time_left
		start()
	else: start()
