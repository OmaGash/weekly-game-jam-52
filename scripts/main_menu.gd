extends Control

export var version = "1.0"

onready var anim_play = find_node("anim_play")
onready var anim_quit = find_node("anim_quit")

func _ready():
	get_node("anim").play("fade_from_black")
	find_node("credits").text = "Break Rush"
	find_node("version").text = version
	Input.set_mouse_mode(0)

func _on_play_pressed():
	anim_play.play("pressed")
	get_node("anim").play("fade_to_black")
	yield(get_node("anim"), "animation_finished")
	get_tree().change_scene("res://scenes/game.tscn")

func _on_quit_pressed():
	anim_quit.play("pressed")
	find_node("Popup").popup_centered(get_viewport_rect().size)


func _on_yes_pressed():
	get_tree().quit()


func _on_no_pressed():
	anim_quit.play("normal")
	find_node("Popup").hide()
