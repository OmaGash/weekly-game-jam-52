extends Area2D
#I'm running out of time so I need to rush things and instead of doing this in the ball scene, 
#it would be more convenient to do it in a separate node that implements this easier.

var fall = preload("res://audio/fall.ogg")

func _ready():
	connect("body_entered", self, "enter")

func enter(body):
	if body.is_in_group("wall"): return
	
	var sfx = t.global_find_node("sfx")
	sfx.stream = fall
	sfx.stream.loop = false
	sfx.play()
