extends KinematicBody2D

var ball_scene = preload("res://scenes/ball.tscn")

var mouse_pos
var can_move = true

#Return true if there are no balls currently present
var no_balls = true

signal go

const PADDLE_WIDTH  = 32
const PADDLE_HEIGHT = 8

func _ready():
	set_physics_process(true)
	t.get_main_node().find_node("timer").connect("timeout", self, "time_up")
	Input.set_mouse_mode(1)

func _physics_process(delta):
	#Get the position of the mouse
	mouse_pos = get_global_mouse_position()
	
	#Set the paddle's x position on the mouse' x position
	if can_move:
		
		position.x = mouse_pos.x
		
		#Makes sure that the paddle will not leak out of the viewport
		if   position.x - PADDLE_WIDTH/2 <= get_viewport_rect().position.x:
			position.x = get_viewport_rect().position.x + PADDLE_WIDTH/2
		elif position.x + PADDLE_WIDTH/2 >= get_viewport_rect().end.x:
			position.x = get_viewport_rect().end.x - PADDLE_WIDTH/2
	
	#Spawn the ball
	if Input.is_action_just_pressed("go") and t.get_main_node().find_node("label_life").hp > 0 and no_balls and can_move:
		t.get_main_node().find_node("time").show()
		
		emit_signal("go")
		var ball = ball_scene.instance()
		ball.position = position - Vector2(0,6)
		t.get_main_node().add_child(ball)
		
		t.get_main_node().find_node("label_life").hp -= 1
		
	

func time_up():
	can_move = false
	Input.set_mouse_mode(0)
