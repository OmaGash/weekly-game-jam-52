extends Label

export var hp = 5

func _ready():
	set_process(true)

func _process(delta):
	if hp != 1:
		text = "Balls: "+str(hp)
	else:
		text = "Ball: " + str(hp)