extends KinematicBody2D

export var hp = 3

signal boss_alive
signal boss_dead

func _ready():
	set_process(true)

func _process(delta):
	
	$Label.text = "HP: " + str(hp)
	emit_signal("boss_alive")
	
	if hp <= 0:
		emit_signal("boss_dead")
		t.get_main_node().find_node("label_score").display += 6
		free()
