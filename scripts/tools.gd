extends Node

var score = 0

func _ready():
	pass

func get_main_node():
	return get_tree().get_root().get_child(get_tree().get_root().get_child_count()-1)

func global_find_node(node_name = ""):
	return get_tree().get_root().get_child(get_tree().get_root().get_child_count()-1).find_node(node_name)