extends Node

var level = 1 #setget next_level
var child

var stage = [preload("res://scenes/levels/level_1.tscn"),
			 preload("res://scenes/levels/level_2.tscn"),
			 preload("res://scenes/levels/level_3.tscn"),
			 preload("res://scenes/levels/level_4.tscn"),
			 preload("res://scenes/levels/level_5.tscn")
]

onready var anim = t.global_find_node("anim")
onready var timer= t.global_find_node("timer")

const FADE_IN = "fade_to_black"
const FADE_OUT= "fade_from_black"

signal next_level

func _ready():
	connect("next_level", self, "next_level", [], CONNECT_ONESHOT)
	var play_stage = stage[level-1].instance()
	add_child(play_stage)
	
	anim.play(FADE_OUT)
	set_process(true)

func _process(delta):
	child = get_child(0)
	if child != null:
		
		if get_child(0).get_child_count() <= 0:
			emit_signal("next_level")
			child.free()

func next_level():
	level = min(4, level + 1)
	print(level)
	anim.play(FADE_IN)
	yield(anim, "animation_finished")
	var next_stage = stage[level-1].instance()
	add_child(next_stage)
	t.global_find_node("paddle").find_node("ball"+str(t.global_find_node("label_life").hp))
	timer.wait_time = 5
	timer.stop()
	anim.play(FADE_OUT)
	connect("next_level", self, "next_level", [], CONNECT_ONESHOT)